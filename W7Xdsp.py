#!/usr/bin/env python3
# Copyright (c) 2008-10 Qtrac Ltd. All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 2 of the License, or
# version 3 of the License, or (at your option) any later version. It is
# provided for educational purposes and is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

import sys, csv
from math import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import time
import datetime
import numpy as np
import w7xarchive as archivedb
import matplotlib.pyplot as plt
from matplotlib import dates
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import fancywidgets.pyQtBased.Table as fancyTable
import threading
import queue

class W7Xdsp(QWidget):

    def __init__(self, parent=None):
        super(W7Xdsp, self).__init__(parent)
#        self.initWindow()

        self.initTable()
        self.readButton = QPushButton('read data', self)
        self.timeButton = QPushButton('enter time', self)
        self.latestButton = QPushButton('read todays latest pulse', self)
        self.relative = QCheckBox('Relative time axis', self)
        self.relative.setChecked(True)
        self.raster = QCheckBox('Rasterize PDFs', self)        
#        self.relative = QLineEdit("1")
        self.date = QLineEdit("yyyy-mm-dd")
        self.time = QLineEdit("hh:mm:ss")
        self.legendCol = QLineEdit("10")
        self.yLabels = QLineEdit("data [a.u.]")
        self.triggers = QComboBox()
        self.triggers.addItem("t0")
        self.triggers.addItem("t1")
        self.triggers.addItem("none")
        self.triggers.setCurrentIndex(0)
        self.progIDbox = QLineEdit("yyyymmdd.x")
        self.readprogButton = QPushButton('read progID',self)
        
        layout = QVBoxLayout()
        layout.addWidget(self.Table)
        layout.addWidget(self.readButton)
        layout.addWidget(QLabel("Global date/time"))
        subLayout1 = QHBoxLayout()
        subLayout1.addWidget(self.date)
        subLayout1.addWidget(self.time)
        subLayout1.addWidget(self.timeButton)
        subLayout4 = QHBoxLayout()
#        layout.addLayout(subLayout1)
        subLayout4.addWidget(self.progIDbox)
        subLayout4.addWidget(self.readprogButton)
        subLayout4.addWidget(self.latestButton)
        layout.addLayout(subLayout4)
        subLayout2 = QHBoxLayout()
        subLayout2.addWidget(self.relative)
        subLayout2.addWidget(self.raster)
        subLayout2.addWidget(QLabel("Trigger source:"))
        subLayout2.addWidget(self.triggers)
        layout.addLayout(subLayout2)
        subLayout3 = QHBoxLayout()
        subLayout3.addWidget(QLabel("y-axis labels:"))
        subLayout3.addWidget(self.yLabels)
        subLayout3.addWidget(QLabel("Columns for legend:"))
        subLayout3.addWidget(self.legendCol)
        layout.addLayout(subLayout3)
        self.setLayout(layout)
        self.readButton.clicked.connect(self.updateUi)
        self.timeButton.clicked.connect(self.EnterTime)
        self.latestButton.clicked.connect(self.getLatestPulse)
        self.readprogButton.clicked.connect(self.readProgID)
        self.setWindowTitle("ArchiveDB plotter")

    def updateUi(self):
        self.readData()
#        self.initWindow()

#    def initWindow(self):
        self.GraphicsWindow = QWidget()
        self.GraphicsWindow.setGeometry(10,10,1000,800)
        # a figure instance to plot on
        self.nPlots = 1
        self.nCols = 1
        self.nRows = 1
        for row in range(self.Table.rowCount()):
#            pStr = self.Table.item(row,10).text()
            rowStr = self.Table.item(row,11).text()
            colStr = self.Table.item(row,12).text()
#            self.nPlots = max(self.nPlots, int(pStr))    
            if (int(rowStr) > 0):
                self.nCols = max(self.nCols, int(colStr))    
            self.nRows = max(self.nRows, int(rowStr))    
        self.nPlots = self.nRows * self.nCols
#        self.figure, self.ax = plt.subplots(self.nPlots, sharex=True)
        self.figure, self.ax = plt.subplots(self.nRows,self.nCols, sharex=True)
        plt.subplots_adjust(left=0.06, right=0.96, top=0.99, bottom=0.1, wspace=0.1)
        try:
            len(self.ax)
        except TypeError:
        # ax is not an array
                self.ax = np.array(self.ax)
        
        self.ax = self.ax.reshape(self.nRows,self.nCols)
        print()
        self.figure.subplots_adjust(hspace=0)
        if self.relative.isChecked(): 
#            xtick_formatter.scaled[1/(24.*60.)]="%S.%f"
            xlabel = "time [s]"
        else:          
            xlabel = "time"
            xtick_locator = dates.AutoDateLocator(interval_multiples=True)
            xtick_formatter = dates.AutoDateFormatter(xtick_locator)
            self.figure.autofmt_xdate()
            self.ax[0,0].xaxis.set_major_locator(xtick_locator)
            self.ax[0,0].xaxis.set_major_formatter(xtick_formatter)
        yLabels=np.resize(np.array(self.yLabels.text().split(',')),self.nPlots)
        yLabels=yLabels.reshape(self.nRows,self.nCols)
        for axRow,labelRow in zip(self.ax,yLabels):
            for ax,label in zip(axRow,labelRow):
                ax.set_ylabel(label)
                ax.set_xlabel(xlabel)
                ax.xaxis.grid()
        
        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)
    
        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # set the layout
        layout = QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        self.GraphicsWindow.setLayout(layout)


        color=plt.cm.rainbow(np.linspace(0,1,32))
        legColumns = self.legendCol.text().split(',')
        
        for i in range(self.data_queue.qsize()):
            data = self.data_queue.get()
            row = data[0]
            axRow = int(self.Table.item(row,11).text())
            if (data[3] and (axRow > 0)):
                axRow = int(self.Table.item(row,11).text())
                axCol = int(self.Table.item(row,12).text())
                colorStr = self.Table.item(row,13).text()
                lineStyleStr = self.Table.item(row,14).text()
                if colorStr.isdigit():
                        c=color[int(colorStr)]
                else: 
                        c=colorStr
                label = ''
                for col in legColumns:
                    label = label + self.Table.item(row,int(col)-1).text() + " "
                self.ax[axRow-1,axCol-1].plot(data[1],data[2],label=label,c=c, linestyle=lineStyleStr, rasterized=self.raster.isChecked())


#        for row in range(self.Table.rowCount()):
#            axRow = int(self.Table.item(row,10).text())
#            if (self.readSucc[row] and (axRow > 0)):
#                axRow = int(self.Table.item(row,10).text())
#                axCol = int(self.Table.item(row,11).text())
#                colorStr = self.Table.item(row,12).text()
#                lineStyleStr = self.Table.item(row,13).text()
#                if colorStr.isdigit():
#                        c=color[int(colorStr)]
#                else: 
#                        c=colorStr
#                label = ''
#                for col in legColumns:
#                    label = label + self.Table.item(row,int(col)-1).text() + " "
#                self.ax[axRow-1,axCol-1].plot(self.tVec[row],self.data[row],label=label,c=c, linestyle=lineStyleStr)
#
        for i in range(self.nRows):
            for j in range(self.nCols):
                self.ax[i,j].legend(prop={'size':8})
        self.canvas.draw()        
        self.GraphicsWindow.show()            


        

    def initTable(self):
        self.Table = fancyTable.Table(rows=1,cols=18,colFiled=True)
        self.Table.resize(2400, 250)
        self.Table.setHorizontalHeaderLabels(("Date","Time","length","trigger offset","Archive","View","Project","Stream Group","Stream","Channel","scaled", "row","col","color","linestyle","offset","factor", "lock row"))
    
        # set data
        self.Table.setItem(0,0, QTableWidgetItem("2015-12-15"))
        self.Table.setItem(0,1, QTableWidgetItem("17:40:00"))
        self.Table.setItem(0,2, QTableWidgetItem("2"))
        self.Table.setItem(0,3, QTableWidgetItem("60"))
        self.Table.setItem(0,4, QTableWidgetItem("ArchiveDB"))        
        self.Table.setItem(0,5, QTableWidgetItem("codac"))
        self.Table.setItem(0,6, QTableWidgetItem("W7X"))
        self.Table.setItem(0,7, QTableWidgetItem("ControlStation.2157"))        
        self.Table.setItem(0,8, QTableWidgetItem("Launcher5.4_DATASTREAM"))
        self.Table.setItem(0,9, QTableWidgetItem("13/L5_ECA61"))
        self.Table.setItem(0,10, QTableWidgetItem("scaled"))
        self.Table.setItem(0,11, QTableWidgetItem("1"))
        self.Table.setItem(0,12, QTableWidgetItem("1"))
        self.Table.setItem(0,13, QTableWidgetItem("k"))
        self.Table.setItem(0,14, QTableWidgetItem("-"))
        self.Table.setItem(0,15, QTableWidgetItem("0"))
        self.Table.setItem(0,16, QTableWidgetItem("1"))
        self.Table.setItem(0,17, QTableWidgetItem("1"))
        #        self.Table.horizontalHeader().setResizeMode(QHeaderView.Stretch)
        self.Table.resizeColumnsToContents()

    def run_parallel_in_threads(self,target, args_list):
        result = queue.Queue()
        # wrapper to collect return value in a Queue
        def task_wrapper(*args):
            result.put(target(*args))
        threads = [threading.Thread(target=task_wrapper, args=args) for args in args_list]
        for t in threads:
            t.start()
        for t in threads:
            t.join()
        return result
    
    
    def get_signal_thread(self,row,url,fr,to, relative):
        try:
            print(url,fr,to)            
            intervals = archivedb.get_time_intervals(url,fr,to)
            times, values = archivedb.get_signal_multiinterval(url, intervals, use_cache=True)
#            times, values = archivedb.get_signal(url, fr, to, useCache=True)
            print(times.shape)
            times = times[np.nonzero(times)]
            offset=float(self.Table.item(row,15).text())
            factor=float(self.Table.item(row,16).text())            
            values = offset + values[np.nonzero(times)]*factor
            tStamp=[]
            if relative: 
                tStamp = (times - archivedb.to_timestamp(fr))*1e-9
            else:
                for t in (times*1e-9):
                    t0 = datetime.datetime.fromtimestamp(t)
                    tStamp.append(t0)

            return (row,tStamp,values,True)
        except Exception as e:
            print("Error reading row",row,e)
            return (row,[0],[0],False)
    
    def readData(self):
    
        self.data=[]
        self.tVec=[]
        self.readSucc=[]
        self.read_args=[]
        for row in range(self.Table.rowCount()):
            print(row)
            if not (int(self.Table.item(row,11).text()) == 0):
                try:
                    dateStr=self.Table.item(row,0).text()
                    db = self.Table.item(row,4).text()
                    view = self.Table.item(row,5).text()
                    project = self.Table.item(row,6).text()
                    streamGroup = self.Table.item(row,7).text()
                    stream = self.Table.item(row,8).text()
                    channel = self.Table.item(row,9).text()
                    scaled = self.Table.item(row,10).text()
                    url = db +"/" + view + "/" + project + "/" + streamGroup + "/" + \
                        stream + "/" + channel  + "/" + scaled
    
                    timeStr=self.Table.item(row,1).text()
                    length=float(self.Table.item(row,2).text())
                    offset = float(self.Table.item(row,3).text())
                    #signal = "ArchiveDB/codac/W7X/FastControlStationDesc.5/PatternRecognitionProcessDesc.30_PARLOG/parms/W7XDbNamedObject/sysText/"
#                    triggers = {
#                        "Program label" : "ArchiveDB/codac/W7X/ProjectDesc.1/ProgramLabelStart/parms/W7XDbNamedObject/sysText", 
#                        "Scenario label" : "ArchiveDB/codac/W7X/ProjectDesc.1/ScenarioLabelStart/parms/W7XDbNamedObject/sysText", 
#                        "Segment label" : "ArchiveDB/codac/W7X/ProjectDesc.1/SegmentLabelStart/parms/W7XDbNamedObject/sysText", 
#                        "Data block" : url,
#                        "none" : "none"}
                    signal = self.triggers.currentText()
                    if signal == "t0":
                        trigger0 = archivedb.get_program_t0(self.progIDbox.text())
#                       trigger0 = (archivedb.get_time_intervals(signal, dateStr + " " + timeStr,archivedb.to_stringdate(archivedb.to_timestamp(dateStr + " " + timeStr)+600e9)))[-1,0]
                    elif signal == "t1":
                        trigger0 = archivedb.get_program_t1(self.progIDbox.text())
                    else:
                        trigger0 = archivedb.to_timestamp(dateStr + " " + timeStr)
                    fr = trigger0+int(offset*1e9)#int(60e9)
                    upto = fr + int(length*1e9)
                    print(url,archivedb.to_stringdate(fr),archivedb.to_stringdate(upto))
                    self.read_args.append((row,url,archivedb.to_stringdate(fr),archivedb.to_stringdate(upto),self.relative.isChecked()))
#                    self.read_args.append((row,url,fr,upto,self.relative.isChecked()))
    
    
    #                    times, values = archivedb.get_signal(url, fr, upto)
    #                    times = times[np.nonzero(times)]
    #                    values = values[np.nonzero(times)]
    #                    self.data.append(values)
    #                    tStamp=[]
    #                    if (): 
    #                        tStamp = (times - fr)*1e-9
    #                    else:
    #                        for t in (times*1e-9):
    #                            t0 = datetime.datetime.fromtimestamp(t)
    #                            tStamp.append(t0)
    #                    self.tVec.append(np.asarray(tStamp))
    #                    self.readSucc.append(True)
                except Exception as e:
                    print("Error reading row",row,e)
    #                    self.readSucc.append(False)
    #                    self.data.append(0)
    #                    self.tVec.append(0)
#            else: 
#                self.readSucc.append(False)
#                self.data.append(0)
#                self.tVec.append(0)

        self.data_queue = self.run_parallel_in_threads(self.get_signal_thread,self.read_args)
        print(self.data_queue)
    
    
    def EnterTime(self):
        for row in range(self.Table.rowCount()):
            dateStr=self.Table.item(row,0).text()        
            if dateStr:
                self.Table.setItem(row, 0, QTableWidgetItem(self.date.text()))
                self.Table.setItem(row, 1, QTableWidgetItem(self.time.text()))

    def getLatestPulse(self):
#        triggers = {
#            "Program label" : "ArchiveDB/codac/W7X/ProjectDesc.1/ProgramLabelStart/parms/W7XDbNamedObject/sysText", 
#            "Scenario label" : "ArchiveDB/codac/W7X/ProjectDesc.1/ScenarioLabelStart/parms/W7XDbNamedObject/sysText", 
#            "Segment label" : "ArchiveDB/codac/W7X/ProjectDesc.1/SegmentLabelStart/parms/W7XDbNamedObject/sysText", 
#            "Data block" : "none",
#            "none" : "none"}
#        signal = triggers [self.triggers.currentText()]
#        if not (signal == "none"):
        progList = archivedb.get_program_list_for_day(datetime.date.today().strftime('%Y-%m-%d'))
        trigger = datetime.datetime.utcfromtimestamp(progList[-1]['trigger']['0'][0]/1e9)
#        trigger = datetime.datetime.fromtimestamp(archivedb.get_latest_time_interval(signal)[0]/1e9)
        self.progIDbox.setText(progList[-1]['id'])
        for row in range(self.Table.rowCount()):
            self.Table.setItem(row, 0, QTableWidgetItem(trigger.strftime('%Y-%m-%d')))
            self.Table.setItem(row, 1, QTableWidgetItem(trigger.strftime('%H:%M:%S')))
        self.updateUi()

    def readProgID(self):
#        triggers = {
#            "Program label" : "ArchiveDB/codac/W7X/ProjectDesc.1/ProgramLabelStart/parms/W7XDbNamedObject/sysText", 
#            "Scenario label" : "ArchiveDB/codac/W7X/ProjectDesc.1/ScenarioLabelStart/parms/W7XDbNamedObject/sysText", 
#            "Segment label" : "ArchiveDB/codac/W7X/ProjectDesc.1/SegmentLabelStart/parms/W7XDbNamedObject/sysText", 
#            "Data block" : "none",
#            "none" : "none"}
#        signal = triggers [self.triggers.currentText()]
#        if not (signal == "none"):

        trigger = datetime.datetime.utcfromtimestamp(archivedb.get_program_t0(self.progIDbox.text())/1e9)
#        trigger = datetime.datetime.fromtimestamp(archivedb.get_latest_time_interval(signal)[0]/1e9)
        for row in range(self.Table.rowCount()):
            self.Table.setItem(row, 0, QTableWidgetItem(trigger.strftime('%Y-%m-%d')))
            self.Table.setItem(row, 1, QTableWidgetItem(trigger.strftime('%H:%M:%S')))
        self.updateUi()

app = QApplication(sys.argv)
gyplot = W7Xdsp()
gyplot.resize(1100,600)
gyplot.show()
app.exec_()

