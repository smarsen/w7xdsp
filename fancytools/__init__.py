'''
a collection of useful non-GUI tools
'''


__version__ = '0.2.0'
__author__ = 'Karl Bedrich'
__email__ = 'karl@bedrich.de'
__url__ = 'http://radjkarl.github.io/fancyTools'
__license__ = 'GPLv3'
__description__ = __doc__
__depencies__= [
		"ordereddict >= 1.1",
		"numpy >= 1.7.1",
	]
__classifiers__ = [
		'Intended Audience :: Developers',
		'Intended Audience :: Science/Research',
		'Intended Audience :: Other Audience',
		'License :: OSI Approved :: GNU General Public License (GPL)',
		'Operating System :: OS Independent',
		'Programming Language :: Python',
		'Topic :: Scientific/Engineering :: Information Analysis',
		'Topic :: Scientific/Engineering :: Visualization',
		'Topic :: Software Development :: Libraries :: Python Modules',
		]