'''
Various QT-based widgets for every day usage.
'''
__version__ = '0.2.5'
__author__ = 'Karl Bedrich'
__email__ = 'karl@bedrich.de'
__url__ = 'https://github.com/radjkarl/fancyWidgets'
__license__ = 'GPLv3'
